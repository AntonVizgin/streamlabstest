# Streamlabs Mobile App Assignment

## Documentation & Thought Process

- So this test assignment took about 4-5 hours, but I wanted to do it in the best way.
- To make the project be scalable in the future in the best way.
- Everything is clear, but I did the feed page in a bit another but more fancy way.
- The `https` was unable to load because the websites `livestreamfails` certificate is invalid, that's why I had to change `https` to `http` in the `videos.json` file.
- It does.