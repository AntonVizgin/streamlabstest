//
//  AccountProviderService.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 16.09.2021.
//

import Foundation
import Combine

final class AccountProviderService: AccountProviderServiceProtocol {
    
    static let shared = AccountProviderService()
    
    private(set) var account: Account?
    
    func getAccount() -> Future<Void, AccountProviderError> {
        Future<Void, AccountProviderError> { promise in
            guard let accountResponse: Account = Bundle.main.loadJSON(.user) else { promise(.failure(.unableToLoad)); return }
            AccountProviderService.shared.account = accountResponse
            promise(.success)
        }
    }
}
