//
//  AccountProviderServiceProtocol.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 16.09.2021.
//

import Foundation
import Combine

enum AccountProviderError: Error {
    case unableToLoad
}

protocol AccountProviderServiceProtocol {
    var account: Account? { get }
    func getAccount() -> Future<Void, AccountProviderError>
}
