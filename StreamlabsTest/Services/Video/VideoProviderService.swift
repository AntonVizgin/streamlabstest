//
//  VideoProviderService.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import Foundation
import Combine

final class VideoProviderService: VideoProviderServiceProtocol {
    
    static let shared = VideoProviderService()
    
    private(set) var videos: [Video] = []
    
    func getVideos() -> Future<Void, VideoProviderError> {
        Future<Void, VideoProviderError> { promise in
            guard let videoResponse: VideoResponse = Bundle.main.loadJSON(.videos) else { promise(.failure(.errorDecoding)); return }
            VideoProviderService.shared.videos = videoResponse.videos
            promise(.success)
        }
    }
}
