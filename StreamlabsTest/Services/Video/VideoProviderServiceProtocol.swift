//
//  VideoProviderServiceProtocol.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit
import Combine

enum VideoProviderError: Error {
    case errorDecoding
    case unableToLoad
}

protocol VideoProviderServiceProtocol {
    var videos: [Video] { get }
    func getVideos() -> Future<Void, VideoProviderError>
}
