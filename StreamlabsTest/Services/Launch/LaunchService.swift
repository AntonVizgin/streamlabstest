//
//  LaunchService.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import Foundation
import Combine

final class LaunchService: LaunchServiceProtocol {
    
    enum LaunchServiceError: Error {
        case unableToLaunch
    }
    
    private let videoService: VideoProviderServiceProtocol
    
    private var observers: Set<AnyCancellable> = []
    
    init() {
        videoService = VideoProviderService.shared
    }
    
    func launch(completion: @escaping (Result<Void, LaunchServiceError>) -> Void) {
        videoService.getVideos()
            .sink(receiveCompletion: { receiveCompletion in
                switch receiveCompletion {
                case .finished: break
                case .failure(_): completion(.failure(.unableToLaunch))
                }
            }, receiveValue: {
                completion(.success)
            })
            .store(in: &observers)
    }
}
