//
//  LaunchServiceProtocol.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import Foundation

protocol LaunchServiceProtocol {
    func launch(completion: @escaping (Result<Void, LaunchService.LaunchServiceError>) -> Void)
}
