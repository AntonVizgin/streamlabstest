//
//  PlayerView.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 16.09.2021.
//

import AVFoundation
import UIKit

final class PlayerView: UIView {
    
    private let player = AVPlayer()
    private var playerLayer: AVPlayerLayer!
    
    private let activityIndicator = ActivityIndicator(.large)
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureView()
        configureLayout()
        
        subscribeForPlayerStatusUpdate()
        subscribeForPlayerEnd()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
        NotificationCenter.default.removeObserver(player, forKeyPath: "status")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        playerLayer.frame = CGRect(x: 0, y: 0, width: layer.frame.width, height: layer.frame.height)
    }
}

// MARK: - UIConfigurable
extension PlayerView: UIConfigurable {
    func configureView() {
        let rootLayer: CALayer = self.layer
        rootLayer.masksToBounds = true

        player.volume = 0
        
        playerLayer = AVPlayerLayer(player: player)
        playerLayer.videoGravity = .resizeAspectFill
        rootLayer.insertSublayer(playerLayer, at: 0)
        
        activityIndicator.hidesWhenStopped = true
        activityIndicator.color = .white
        activityIndicator.startAnimating()
    }
    
    func configureLayout() {
        addSubview(activityIndicator)
        
        NSLayoutConstraint.activate(
            [
                activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
                activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
            ]
        )
    }
    
    func prepateForReuse() {
        player.pause()
        player.replaceCurrentItem(with: nil)
    }
}

// MARK: - Controls
extension PlayerView {
    func setContent(url: URL) {
        let item = AVPlayerItem(url: url)
        player.replaceCurrentItem(with: item)
        
        play()
    }
    
    func play() {
        if player.currentItem != nil {
            player.play()
        }
    }
    
    func pause() {
        player.pause()
    }
}

// MARK: - Subscriptions
extension PlayerView {
    func subscribeForPlayerStatusUpdate() {
        player.addObserver(self, forKeyPath: "status", options: .initial, context: nil)
    }
    
    func subscribeForPlayerEnd() {
        NotificationCenter.default.addObserver(forName: .AVPlayerItemDidPlayToEndTime, object: nil, queue: nil) { [weak self] _ in
            self?.pause()
            self?.player.seek(to: .zero)
            self?.play()
        }
    }
    
    @objc private func handle(_ notification: Notification) {
        
    }
    
    override func observeValue(forKeyPath keyPath: String?,
                               of object: Any?,
                               change: [NSKeyValueChangeKey : Any]?,
                               context: UnsafeMutableRawPointer?) {
        
        guard let keyPath = keyPath, keyPath == "status" else { return }
        guard let object = object as? AVPlayer else { return }
        guard object == player else { return }
        
        switch player.status {
        case .readyToPlay:
            activityIndicator.stopAnimating()
            play()
        case .failed: print("FAILED TO PLAY")
        case .unknown: break
        @unknown default: break
        }
    }
}
