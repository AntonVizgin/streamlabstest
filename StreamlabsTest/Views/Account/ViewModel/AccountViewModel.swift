//
//  AccountViewModel.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 16.09.2021.
//

import Foundation
import Combine

final class AccountViewModel: NSObject, ObservableObject {
    
    enum Event {
        case didTapEditProfile
        case didTapFollowing
        case didTapFans
        case didTapHearts
    }
    
    enum State {
        case idle
        case loading
        case completed
        case error
    }
    
    let eventProvider: PassthroughSubject<Event, Never> = .init()
    @Published var loadingState: State = .idle
    
    private let accountService: AccountProviderServiceProtocol = AccountProviderService.shared
    private var observers: Set<AnyCancellable> = []
}

// MARK - Variables
extension AccountViewModel {
    var account: Account? {
        accountService.account
    }
}

// MARK: - Actions
extension AccountViewModel {
    func loadData() {
        guard accountService.account == nil else { return }
        loadingState = .loading
        
        accountService.getAccount()
            .sink(
                receiveCompletion: { [weak self] in
                    switch $0 {
                    case .finished: break
                    case .failure(_): self?.loadingState = .error
                    }
                },
                receiveValue: { [weak self] in
                    DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                        self?.loadingState = .completed
                    }
                }
            )
            .store(in: &observers)
    }
    
    func setState(_ state: State) {
        self.loadingState = state
    }
}
