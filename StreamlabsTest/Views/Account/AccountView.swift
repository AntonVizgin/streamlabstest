//
//  AccountView.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 16.09.2021.
//

import UIKit

final class AccountView: UIView {
    
    private lazy var activityIndicator = ActivityIndicator()
    
    private let userInfoStackView = UIStackView()
    
    private let userImageView = UIImageView()
    private let userNameLabel = UILabel()
    private let numberOfVideosLabel = UILabel()
    
    private let socialDetailsStackView = UIStackView()
    private let followingsCountLabel = UILabel()
    private let fansCountLabel = UILabel()
    private let heartsCountLabel = UILabel()
    private let bioLabel = UILabel()
    
    let editBioButton = UIButton()
    
    init(_ frame: CGRect = .zero) {
        super.init(frame: .zero)
        
        configureView()
        configureLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - UIConfigurable
extension AccountView: UIConfigurable {
    func configureView() {
        backgroundColor = .white
        layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        userInfoStackView.translatesAutoresizingMaskIntoConstraints = false
        userInfoStackView.axis = .vertical
        userInfoStackView.spacing = 10
        userInfoStackView.alignment = .center
        
        userImageView.layer.cornerRadius = 40
        userImageView.layer.masksToBounds = true
        
        userNameLabel.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        
        numberOfVideosLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        numberOfVideosLabel.textColor = .lightGray
        
        socialDetailsStackView.translatesAutoresizingMaskIntoConstraints = false
        socialDetailsStackView.axis = .horizontal
        socialDetailsStackView.spacing = 40
        
        [followingsCountLabel, fansCountLabel, heartsCountLabel].forEach {
            $0.font = UIFont.systemFont(ofSize: 16, weight: .regular)
            $0.numberOfLines = 0
            $0.textAlignment = .center
        }
        
        editBioButton.backgroundColor = .red
        editBioButton.setTitle("Edit profile", for: .normal)
        editBioButton.titleLabel?.font = UIFont.systemFont(ofSize: 16, weight: .bold)
        editBioButton.contentEdgeInsets = UIEdgeInsets(top: 15, left: 15, bottom: 15, right: 15)
        
        bioLabel.font = UIFont.systemFont(ofSize: 16, weight: .regular)
        bioLabel.textColor = .lightGray
    }
    
    func configureLayout() {
        addSubview(userInfoStackView)
        addSubview(activityIndicator)
        
        userInfoStackView.addArrangedSubview(userImageView)
        userInfoStackView.addArrangedSubview(userNameLabel)
        userInfoStackView.addArrangedSubview(numberOfVideosLabel)
        userInfoStackView.setCustomSpacing(20, after: numberOfVideosLabel)
        userInfoStackView.addArrangedSubview(socialDetailsStackView)
        userInfoStackView.setCustomSpacing(30, after: socialDetailsStackView)
        userInfoStackView.addArrangedSubview(editBioButton)
        userInfoStackView.setCustomSpacing(30, after: editBioButton)
        userInfoStackView.addArrangedSubview(bioLabel)
        
        socialDetailsStackView.addArrangedSubview(followingsCountLabel)
        socialDetailsStackView.addArrangedSubview(fansCountLabel)
        socialDetailsStackView.addArrangedSubview(heartsCountLabel)
        
        NSLayoutConstraint.activate(
            [
                activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
                activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor),
                
                userInfoStackView.topAnchor.constraint(equalTo: layoutMarginsGuide.topAnchor),
                userInfoStackView.bottomAnchor.constraint(lessThanOrEqualTo: layoutMarginsGuide.bottomAnchor),
                userInfoStackView.leftAnchor.constraint(equalTo: layoutMarginsGuide.leftAnchor),
                userInfoStackView.rightAnchor.constraint(equalTo: layoutMarginsGuide.rightAnchor),
                
                userImageView.widthAnchor.constraint(equalToConstant: 80),
                userImageView.heightAnchor.constraint(equalToConstant: 80)
            ]
        )
    }
}

// MARK: - Setup Data
extension AccountView {
    func startAnimating() {
        userInfoStackView.isHidden = true
        activityIndicator.startAnimating()
    }
    
    func stopAnimating() {
        userInfoStackView.isHidden = false
        activityIndicator.stopAnimating()
    }
    
    func setup(account: Account?) {
        guard let account = account else { return }
        
        userImageView.image = UIImage(named: account.image)
        userNameLabel.text = account.userName
        numberOfVideosLabel.text = "\(account.videosCount) Videos"
        
        followingsCountLabel.text = "\(account.followersCount)\nFollowing"
        fansCountLabel.text = "\(account.fansCount)\nFans"
        heartsCountLabel.text = "\(account.heartsCount)\nHearts"
        
        bioLabel.text = "No bio yet"
    }
}
