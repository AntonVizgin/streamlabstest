//
//  AccountViewController.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 16.09.2021.
//

import UIKit
import SwiftUI
import Combine

final class AccountViewController: UIViewController {
    
    private let accountView = AccountView()
    
    @ObservedObject private var viewModel: AccountViewModel
    private var observers: Set<AnyCancellable> = []
    
    init(viewModel: AccountViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = accountView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configureLayout()
        
        subscribeForLoadingStateUpdates()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        viewModel.loadData()
    }
}

// MARK: - UIConfigurable
extension AccountViewController: UIConfigurable {
    func configureView() {
        navigationItem.title = "Account"
        
        
    }
    
    func configureLayout() { }

    func setupData() {
        accountView.setup(account: viewModel.account)
    }
}

// MARK: - Subscriptions
private extension AccountViewController {
    func subscribeForLoadingStateUpdates() {
        viewModel.$loadingState
            .sink { [weak self] in
                switch $0 {
                case .idle: break
                case .loading: self?.accountView.startAnimating()
                case .completed:
                    self?.accountView.stopAnimating()
                    self?.setupData()
                case .error: self?.accountView.stopAnimating()
                }
            }
            .store(in: &observers)
    }
}
