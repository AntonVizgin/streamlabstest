//
//  LaunchViewController.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit
import Combine

final class LaunchViewController: UIViewController {
    
    private let launchView = LaunchView()

    override func loadView() {
        view = launchView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension LaunchViewController {
    func setState(_ state: LaunchView.State) {
        launchView.setState(state)
    }
}


