//
//  LaunchView.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class LaunchView: UIView {
    
    enum State {
        case idle
        case loading
        case error
    }
    
    private lazy var activityIndicator = ActivityIndicator()
    
    private var state: State = .idle {
        didSet {
            switch state {
            case .loading: activityIndicator.startAnimating()
            case .idle, .error: activityIndicator.stopAnimating()
            }
        }
    }
    
    init(_ frame: CGRect = .zero) {
        super.init(frame: frame)
        
        configureView()
        configureLayout()
        
        setState(.loading)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// MARK: - Configuration
extension LaunchView: UIConfigurable {
    func configureView() {
        backgroundColor = .white
    }
    
    func configureLayout() {
        addSubview(activityIndicator)
        
        NSLayoutConstraint.activate(
            [
                activityIndicator.centerXAnchor.constraint(equalTo: centerXAnchor),
                activityIndicator.centerYAnchor.constraint(equalTo: centerYAnchor)
            ]
        )
    }
}

// MARK: - Actions
extension LaunchView {
    func setState(_ state: State) {
        self.state = state
    }
}
