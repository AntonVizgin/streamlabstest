//
//  FeedView.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class FeedView: UIView {
    
    private lazy var flowLayout: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 0
        layout.minimumInteritemSpacing = 0
        return layout
    }()
    
    private lazy var collectionView: UICollectionView = {
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.translatesAutoresizingMaskIntoConstraints = false
        return collectionView
    }()
    
    init(_ frame: CGRect = .zero) {
        super.init(frame: frame)
        
        configureView()
        configureLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func reloadData() {
        collectionView.reloadData()
    }
}

// MARK: - UIConfig
extension FeedView: UIConfigurable {
    func configureView() {
        backgroundColor = .white
        collectionView.backgroundColor = .white
        
        collectionView.register(FeedVideoCell.self, forCellWithReuseIdentifier: FeedVideoCell.reuseIdentifier)
    }
    
    func configureLayout() {
        addSubview(collectionView)
        
        NSLayoutConstraint.activate(
            [
                collectionView.topAnchor.constraint(equalTo: topAnchor),
                collectionView.bottomAnchor.constraint(equalTo: bottomAnchor),
                collectionView.leftAnchor.constraint(equalTo: leftAnchor),
                collectionView.rightAnchor.constraint(equalTo: rightAnchor)
            ]
        )
    }
}

// MARK: -
extension FeedView {
    func set(dataSource: UICollectionViewDataSource) {
        collectionView.dataSource = dataSource
    }
    
    func set(delegate: UICollectionViewDelegateFlowLayout) {
        collectionView.delegate = delegate
    }
}
