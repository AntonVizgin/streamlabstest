//
//  FeedViewController.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class FeedViewController: UIViewController {
    
    private let viewModel: FeedViewModel
    private let feedView = FeedView()
    
    init(viewModel: FeedViewModel) {
        self.viewModel = viewModel
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = feedView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        configureLayout()
    }
}

extension FeedViewController: UIConfigurable {
    func configureView() {
        navigationItem.title = "Feed"
        
        feedView.set(dataSource: viewModel)
        feedView.set(delegate: viewModel)
        feedView.reloadData()
    }
    
    func configureLayout() { }
}
