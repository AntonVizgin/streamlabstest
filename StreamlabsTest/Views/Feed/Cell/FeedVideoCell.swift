//
//  FeedVideoCell.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class FeedVideoCell: UICollectionViewCell {
    
    static let reuseIdentifier = String(describing: self)
    
    private lazy var backgroundPlayerView = PlayerView()
    private lazy var backgroundDimmView = UIView()
    
    private lazy var infoStackView = UIStackView()
    private lazy var videoInfoStackView = UIStackView()
    private lazy var socialInfoStackView = UIStackView()
    
    private lazy var userImageView = UIImageView()
    private lazy var videoDescriptionLabel = UILabel()
    
    private lazy var commentsCountLabel = UILabel()
    private lazy var likesCountLabel = UILabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configureView()
        configureLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func configure(with item: Video) {
        userImageView.image = UIImage(named: item.userImage)
        videoDescriptionLabel.text = item.videoDescription
        commentsCountLabel.text = "Comments: \(item.numberOfComments)"
        likesCountLabel.text = "Likes: \(item.numberOfLikes)"
        
        backgroundPlayerView.setContent(url: item.videoURL)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        backgroundPlayerView.prepateForReuse()
    }
}

extension FeedVideoCell: UIConfigurable {
    func configureView() {
        contentView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        
        contentView.layer.cornerRadius = 10
        contentView.layer.shadowColor = UIColor.black.cgColor
        contentView.layer.shadowOpacity = 0.4
        contentView.layer.shadowOffset = .zero
        contentView.layer.shadowRadius = 5
        
        backgroundPlayerView.translatesAutoresizingMaskIntoConstraints = false
        backgroundPlayerView.layoutMargins = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
        backgroundPlayerView.layer.cornerRadius = 10
        
        backgroundDimmView.translatesAutoresizingMaskIntoConstraints = false
        backgroundDimmView.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        
        infoStackView.translatesAutoresizingMaskIntoConstraints = false
        infoStackView.axis = .vertical
        infoStackView.spacing = 5
        infoStackView.isLayoutMarginsRelativeArrangement = true
        
        videoInfoStackView.translatesAutoresizingMaskIntoConstraints = false
        videoInfoStackView.axis = .horizontal
        videoInfoStackView.alignment = .center
        videoInfoStackView.distribution = .fill
        videoInfoStackView.spacing = 15
        
        
        userImageView.translatesAutoresizingMaskIntoConstraints = false
        userImageView.layer.cornerRadius = 20
        userImageView.layer.masksToBounds = true
        
        videoDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
        videoDescriptionLabel.numberOfLines = 0
        videoDescriptionLabel.textColor = .white
        
        socialInfoStackView.translatesAutoresizingMaskIntoConstraints = false
        socialInfoStackView.axis = .vertical
        socialInfoStackView.spacing = 5

        commentsCountLabel.translatesAutoresizingMaskIntoConstraints = false
        commentsCountLabel.textColor = .white
        commentsCountLabel.setContentHuggingPriority(.defaultHigh, for: .horizontal)
        
        likesCountLabel.translatesAutoresizingMaskIntoConstraints = false
        likesCountLabel.textColor = .white
        likesCountLabel.setContentHuggingPriority(.defaultLow, for: .horizontal)
    }
    
    func configureLayout() {
        contentView.addSubview(backgroundPlayerView)
        backgroundPlayerView.addSubview(backgroundDimmView)
        backgroundPlayerView.addSubview(infoStackView)
        
        infoStackView.addArrangedSubview(videoInfoStackView)
        infoStackView.addArrangedSubview(socialInfoStackView)
        
        videoInfoStackView.addArrangedSubview(userImageView)
        videoInfoStackView.addArrangedSubview(videoDescriptionLabel)
        infoStackView.setCustomSpacing(15, after: videoInfoStackView)
        
        socialInfoStackView.addArrangedSubview(commentsCountLabel)
        socialInfoStackView.addArrangedSubview(likesCountLabel)
        
        NSLayoutConstraint.activate(
            [
                backgroundPlayerView.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor),
                backgroundPlayerView.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor),
                backgroundPlayerView.leftAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leftAnchor),
                backgroundPlayerView.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor),
                
                backgroundDimmView.topAnchor.constraint(equalTo: contentView.layoutMarginsGuide.topAnchor),
                backgroundDimmView.bottomAnchor.constraint(equalTo: contentView.layoutMarginsGuide.bottomAnchor),
                backgroundDimmView.leftAnchor.constraint(equalTo: contentView.layoutMarginsGuide.leftAnchor),
                backgroundDimmView.rightAnchor.constraint(equalTo: contentView.layoutMarginsGuide.rightAnchor),
                
                infoStackView.bottomAnchor.constraint(equalTo: backgroundPlayerView.layoutMarginsGuide.bottomAnchor),
                infoStackView.leftAnchor.constraint(equalTo: backgroundPlayerView.layoutMarginsGuide.leftAnchor),
                infoStackView.rightAnchor.constraint(equalTo: backgroundPlayerView.layoutMarginsGuide.rightAnchor),
                
                userImageView.widthAnchor.constraint(equalToConstant: 40),
                userImageView.heightAnchor.constraint(equalToConstant: 40)
            ]
        )
    }
}

