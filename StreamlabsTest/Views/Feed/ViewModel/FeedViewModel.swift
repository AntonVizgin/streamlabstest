//
//  FeedViewModel.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit
import Combine

final class FeedViewModel: NSObject, ObservableObject {
    
    enum Event {
        case didTapOnItem(Video)
    }
    
    let eventProvider: PassthroughSubject<Event, Never> = .init()
    
    private let videoProviderService: VideoProviderServiceProtocol = VideoProviderService.shared
}

// MARK: - Variables
private extension FeedViewModel {
    var numberOfItems: Int { videoProviderService.videos.count }
    func getVideoAtIndex(index: Int) -> Video { videoProviderService.videos[index] }
}

// MARK: - UICollectionViewDataSrouce
extension FeedViewModel: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        numberOfItems
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FeedVideoCell.reuseIdentifier, for: indexPath) as? FeedVideoCell else { fatalError() }
        cell.configure(with: getVideoAtIndex(index: indexPath.item))
        return cell
    }
}

extension FeedViewModel: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        CGSize(width: collectionView.frame.width, height: collectionView.frame.width / 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        eventProvider.send(.didTapOnItem(getVideoAtIndex(index: indexPath.item)))
    }
}
