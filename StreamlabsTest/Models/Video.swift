//
//  Video.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import Foundation

struct VideoResponse: Decodable {
    let videos: [Video]
}

struct Video: Decodable {
    let userId: String
    let userName: String
    let userImage: String
    let videoURL: URL
    let videoDescription: String
    let numberOfLikes: Int
    let numberOfComments: Int
    
    enum CodingKeys: String, CodingKey {
        case userId = "user_id"
        case userName = "user_name"
        case userImage = "user_image_path"
        case videoURL = "video_path"
        case videoDescription = "video_description"
        case numberOfLikes = "video_number_likes"
        case numberOfComments = "video_number_comments"
    }
}
