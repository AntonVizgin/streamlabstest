//
//  Account.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 16.09.2021.
//

import Foundation

struct Account: Decodable {
    let userName: String
    let userTitle: String
    let image: String
    
    let videosCount: Int
    let followersCount: Int
    let fansCount: Int
    let heartsCount: Int
    
    enum CodingKeys: String, CodingKey {
        case userName = "user_name"
        case userTitle = "user_title"
        case image = "user_local_image"
        
        case videosCount = "user_videos"
        case followersCount = "user_following"
        case fansCount = "user_fans"
        case heartsCount = "user_hearts"
    }
}
