//
//  RootCoordinator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

protocol RootViewCoordinator {
    var rootViewController: UIViewController { get }
}
