//
//  TabCoordinator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

protocol TabCoordinator: RootViewCoordinator {
    var tabImage: UIImage { get }
    var tabSelectedImage: UIImage { get }
}

extension TabCoordinator {
    /// Adds a `UITabBarItem` to the coordinator's `rootViewController` using the
    /// `tabTitle` and `tabImage`.
    func configureTabBarItemForRootViewController() {
        let root = rootViewController
        let item = UITabBarItem(title: nil, image: tabImage, selectedImage: tabSelectedImage)
        root.tabBarItem = item
    }
}
