//
//  UIConfigurable.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import Foundation

protocol UIConfigurable {
    func configureView()
    func configureLayout()
}
