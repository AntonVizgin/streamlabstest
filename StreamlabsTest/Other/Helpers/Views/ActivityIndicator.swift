//
//  ActivityIndicator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class ActivityIndicator: UIActivityIndicatorView {
    init(_ style: UIActivityIndicatorView.Style = .large) {
        super.init(style: style)
        
        hidesWhenStopped = true
        translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
