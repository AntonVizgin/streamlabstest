//
//  Bundle+Extras.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

enum MockJSONToLoad: String {
    case videos
    case user
}

extension Bundle {
    func loadJSON<T: Decodable>(_ json: MockJSONToLoad) -> T? {
        guard let path = Bundle.main.path(forResource: json.rawValue, ofType: "json") else { return nil }
        do {
            let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
            return try JSONDecoder().decode(T.self, from: data)
          } catch {
            return nil
          }
    }
}
