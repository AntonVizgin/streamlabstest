//
//  UIWindow+Extras.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

extension UIWindow {
    func setRootViewControllerAnimated(_ viewController: UIViewController?) {
        rootViewController = viewController
        
        UIView.transition(with: self, duration: 0.3, options: .transitionCrossDissolve, animations: nil)
    }
}
