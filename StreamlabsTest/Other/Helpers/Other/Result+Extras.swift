//
//  Result+Extras.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import Foundation

extension Result where Success == Void {
    static var success: Result {
        return .success(())
    }
}
