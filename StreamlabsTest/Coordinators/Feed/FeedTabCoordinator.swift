//
//  FeedTabCoordinator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class FeedTabCoordinator: TabCoordinator {
    
    private let navigationController = UINavigationController()
    
    var tabImage: UIImage { UIImage(systemName: "house")! }
    var tabSelectedImage: UIImage { UIImage(systemName: "house.fill")! }
    
    var rootViewController: UIViewController {
        navigationController
    }
    
    private(set) lazy var feedCoordinator = FeedCoordinator(navigationController: navigationController)
    
    init() {
        navigationController.setViewControllers([feedCoordinator.rootViewController], animated: true)
        configureTabBarItemForRootViewController()
    }
}
