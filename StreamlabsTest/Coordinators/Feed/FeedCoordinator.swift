//
//  FeedCoordinator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit
import Combine

final class FeedCoordinator: RootViewCoordinator {
    
    private let navigationController: UINavigationController
    private let feedViewController: FeedViewController
    
    var rootViewController: UIViewController {
        feedViewController
    }
    
    private var observers: Set<AnyCancellable> = []
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        
        let viewModel = FeedViewModel()
        feedViewController = FeedViewController(viewModel: viewModel)
        
        viewModel.eventProvider
            .sink {
                switch $0 {
                case .didTapOnItem(let video): break // SHOW FULLSCREEN VIDEO
                }
            }
            .store(in: &observers)
    }
}
