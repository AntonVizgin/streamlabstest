//
//  AccountCoordinator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 16.09.2021.
//

import UIKit

final class AccountCoordinator: RootViewCoordinator {
    
    private let navigationController: UINavigationController
    private let accountViewController: UIViewController
    
    var rootViewController: UIViewController {
        accountViewController
    }
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
        
        let viewModel = AccountViewModel()
        self.accountViewController = AccountViewController(viewModel: viewModel)
    }
}
