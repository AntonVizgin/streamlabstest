//
//  AccountTabCoordinator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class AccountTabCoordinator: TabCoordinator {
    
    private let navigationController = UINavigationController()
    
    var tabImage: UIImage { UIImage(systemName: "person.crop.circle")! }
    var tabSelectedImage: UIImage { UIImage(systemName: "person.crop.circle.fill")! }
    
    var rootViewController: UIViewController {
        navigationController
    }
    
    private(set) lazy var acountCoordinator = AccountCoordinator(navigationController: navigationController)
    
    init() {
        navigationController.setViewControllers([acountCoordinator.rootViewController], animated: true)
        configureTabBarItemForRootViewController()
    }
}
