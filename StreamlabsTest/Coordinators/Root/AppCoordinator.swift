//
//  AppCoordinator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class AppCoordinator {
    
    private var window: UIWindow!
    
    private let launcher: LaunchServiceProtocol = LaunchService()
    private let launchViewController = LaunchViewController()
    private var tabBarCoordinator: TabBarCoordinator?
    
    init(scene: UIWindowScene) {
        configureWindow(scene: scene)
        
        prepareForLaunch()
    }

    private func configureWindow(scene: UIWindowScene) {
        window = UIWindow(windowScene: scene)
        window.rootViewController = launchViewController
        window.makeKeyAndVisible()
    }
    
    private func prepareForLaunch() {
        launcher.launch { [weak self] result in
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                switch result {
                case .success: self?.setTabBarRootController()
                case .failure(let error): print(error)
                }
            }
        }
    }
    
    private func setTabBarRootController() {
        tabBarCoordinator = TabBarCoordinator()
        
        window.setRootViewControllerAnimated(tabBarCoordinator?.rootViewController)
    }
}
