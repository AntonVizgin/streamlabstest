//
//  TabCoordinator.swift
//  StreamlabsTest
//
//  Created by Anton Vizgin on 15.09.2021.
//

import UIKit

final class TabBarCoordinator: RootViewCoordinator {
    enum Tab: Int {
        case feed
        case profile
    }

    var tabs: [Tab] {
        [.feed, .profile]
    }

    private let tabBarController = UITabBarController()
    
    var rootViewController: UIViewController {
        tabBarController
    }
    
    init() {
        var tabControllers = [UIViewController]()
        
        for tab in tabs {
            let coordinator: RootViewCoordinator
            switch tab {
            case .feed: coordinator = FeedTabCoordinator()
            case .profile: coordinator = AccountTabCoordinator()
            }
            tabControllers.append(coordinator.rootViewController)
        }
        tabBarController.setViewControllers(tabControllers, animated: false)
    }
}
